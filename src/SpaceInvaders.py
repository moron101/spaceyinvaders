import pygame, random

# Constants
WIN_WIDTH   = 400
WIN_HEIGTH  = 800
WHITE       = (255,255,255)
RED         = (255,0,0)
BLUE        = (0,64,255)
# Global variables
all_sprites_list        = pygame.sprite.Group()
invader_sprites_list    = pygame.sprite.Group()
lasers_sprites_list     = pygame.sprite.Group()
inv_laser_sprites_list  = pygame.sprite.Group()
spaceship               = None
lasers_obj_list         = []
inv_lasers_obj_list     = []
invaders_obj_list       = []
invaders_lateral_pos    = 80
invaders_lateral_dir    = "<"
invaders_vertical_pos   = 0

class Laser(pygame.sprite.Sprite):
    def __init__(self, laser_color, laser_dir, x_axis):
        super().__init__()
        self.image      = pygame.Surface([4,40])
        self.image.fill(WHITE)
        self.image.set_colorkey(WHITE)
        pygame.draw.rect(self.image,laser_color,[0,0,4,40])
        self.rect       = self.image.get_rect()
        self.lsr_dir    = laser_dir
        self.x_axis     = x_axis
        if(laser_dir == "up"):
            self.rect.x = self.x_axis
            self.rect.y = WIN_HEIGTH - 120
        else:
            self.rect.x = self.x_axis
            self.rect.y = 85
    def moveLaser(self):
        if(self.lsr_dir == "up"):
            self.rect.x = self.x_axis
            self.rect.y -= 10
        else:
            self.rect.x = self.x_axis
            self.rect.y += 10
class GameChrCreate(pygame.sprite.Sprite):      # Class to create game characters
    invaders_pic_list = ["cloud1.png","cloud2.png","cyclop1.png","cyclop2.png","cyclop3.png","cyclop4.png","planet1.png","planet2.png","predator1.png","predator2.png","satellites1.png","satellites2.png","space1.png","space2.png"]
    def __init__(self, x_axis, y_axis, player=None):
        super().__init__()
        if player is not None:
            self.image  = pygame.image.load("spaceship.png")
        else:
            self.image  = pygame.image.load(self.invaders_pic_list[random.randrange(0,len(self.invaders_pic_list))])
        self.image  = pygame.transform.scale(self.image,(40,40))
        self.image.set_colorkey(WHITE)
        self.rect   = self.image.get_rect()
        self.rect.x = x_axis
        self.rect.y = y_axis
    def selfMovement(self):
        global invaders_lateral_dir
        global invaders_vertical_pos
        if(invaders_lateral_dir == "<"):
            self.rect.x -= 2
        elif(invaders_lateral_dir == ">"):
            self.rect.x += 2
        if(invaders_vertical_pos == 0):
            self.rect.y += 42

def chrCreationLogic(player=None):              # Method for the how's of character creation
    global spaceship
    if player is not None:
        spaceship = GameChrCreate(WIN_WIDTH/2, WIN_HEIGTH-60,player)
        all_sprites_list.add(spaceship)
    else:
        start_xpos = invaders_lateral_pos
        for i in range(4):
            invader = GameChrCreate(start_xpos,0)
            invaders_obj_list.append(invader)
            invader_sprites_list.add(invader)
            all_sprites_list.add(invader)
            start_xpos += 80
def enumListOfInvaders():
    # Used to move the invaders
    for i in invaders_obj_list:
        i.selfMovement()
def increasePosMarkers():
    global invaders_lateral_pos
    global invaders_vertical_pos
    global invaders_lateral_dir

    if(invaders_lateral_dir == "<") and (invaders_lateral_pos > 5):
        invaders_lateral_pos -= 2
        invaders_vertical_pos += 1
    elif(invaders_lateral_dir == "<") and (invaders_lateral_pos <= 5):                  # change direction and move down a row
        invaders_lateral_dir = ">"
        invaders_vertical_pos = 0
        chrCreationLogic()                                                              # to new row of invaders
    elif(invaders_lateral_dir == ">") and (invaders_lateral_pos < 115):
        invaders_lateral_pos += 2
        invaders_vertical_pos += 1
    elif(invaders_lateral_dir == ">") and (invaders_lateral_pos >= 115):                 # change direction and move down a row
        invaders_lateral_dir = "<"
        invaders_vertical_pos = 0
        chrCreationLogic()                                                              # to new row of invaders
def createListOfLasers(laser_color,laser_dir,x_axis):
    L = Laser(laser_color,laser_dir,x_axis)
    # Separate lists for lasers by plaver and invaders
    if(laser_color == BLUE):
        lasers_obj_list.append(L)
        lasers_sprites_list.add(L)
    else:
        inv_lasers_obj_list.append(L)
        inv_laser_sprites_list.add(L)
    all_sprites_list.add(L)
def enumListOfLasers():
    # Enumerate lasers by player
    for i in lasers_obj_list:
        i.moveLaser()
        if(pygame.sprite.spritecollide(i,invader_sprites_list,True)):
            i.kill()                        # removes the laser sprite
            lasers_obj_list.remove(i)       # we're removing the object so it doesn't continue...
    # Enumerate lasers by invaders
    for a in inv_lasers_obj_list:
        a.moveLaser()
def pauseGame():
    # infinite loop until quit is selected
    while True:
        for event in pygame.event.get():
            if(event.type == pygame.QUIT):
                return False
def gameLogic():
    # Create player character
    chrCreationLogic(player="protagonist")

    # Create invaders character
    chrCreationLogic()
    # Loop
    continue_game = True
    while continue_game:
        # Event listener for the game
        for event in pygame.event.get():
            if(event.type == pygame.KEYDOWN):
                if(event.key == pygame.K_SPACE):
                    createListOfLasers(BLUE,"up",spaceship.rect.x + 30)
            if(event.type == pygame.QUIT):
                continue_game = False
        # Make invaders shoot
        if((random.randrange(0,10)%10 == 0)):
            createListOfLasers(RED,"dn",random.randrange(60,WIN_WIDTH-20))
        # Make lasers travel
        enumListOfLasers()
        # Make invaders move
        enumListOfInvaders()
        # Increase positional markers
        increasePosMarkers()
        # Get mouse position and move hunter
        pos = pygame.mouse.get_pos()
        if((pos[0] > 2) and (pos[0] < WIN_WIDTH - 42)):
            spaceship.rect.x = pos[0]
        # Check is invaders crashed into player
        if(pygame.sprite.spritecollide(spaceship,invader_sprites_list,False)):
            continue_game = pauseGame()
        # Check if invader lasers hit player
        if(pygame.sprite.spritecollide(spaceship,inv_laser_sprites_list,False)):
            continue_game = pauseGame()
        # Draw all Sprites
        all_sprites_list.draw(game_window)
        # Refresh root window
        pygame.display.flip()
        game_window.fill(WHITE)
        # FPS
        pygame.time.Clock().tick(30)

# Initialize Pygame
pygame.init()
# Screen object attributes
game_window = pygame.display.set_mode((WIN_WIDTH,WIN_HEIGTH))
pygame.display.set_caption("Space Invaders, circa 1980\'s")
# State game logic
gameLogic()
# End game
pygame.quit()